<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

         <link href="<?php echo get_template_directory_uri(); ?>/fonts/font.css" rel='stylesheet' type='text/css'>

         <link href="<?php echo get_template_directory_uri(); ?>/js/slick/slick.css" rel='stylesheet' type='text/css'>
         <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/js/slick/slick-theme.css"/>

         <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/slick/slick.min.js"></script>

        <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700' rel='stylesheet' type='text/css'>

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
			
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	</head>
	<body <?php body_class(); ?>>

		<!-- wrapper -->
		<div class="wrapper">

			<!-- header -->
			<header class="header clear" role="banner">

				<div class="contain-header">
					<div class="wrapper-header">
						<div class="top-bar">
							<!-- logo -->
							<div class="logo">
								<a href="<?php echo home_url(); ?>">
									<img src="<?php echo get_template_directory_uri(); ?>/img/logo-paymentlock.png" alt="Logo" class="logo-img">
								</a>
							</div>
							<!-- /logo -->

							<!-- nav -->
							<nav class="nav" role="navigation">
								
								<?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
								<ul id="signin-link">
									<li>
										<a href="https://paymentlock.zendesk.com">
											<button class="btn-signin">Sign In</button>
										</a>
									</li>
								</ul>
							</nav>
							<!-- /nav -->

						</div>
					</div>
				</div>

				<div class="text-header">
					<h2>paymentLOCK <span id="roboto">secures all your <span id="text-underline">card-not-present</span> transactions... </span></h2>
					<p>Proactively detect and diminish your business's susceptibility to credit card fraud</p> 
					<a class="js-open-modal" href="#" data-modal-id="popup"><button class="btn-getstarted-sm">Get Started > </button></a>
				</div>

				
			</header>
			<!-- /header -->
