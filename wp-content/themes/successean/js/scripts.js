$(document).ready(function(){ 
    $(function(){

    var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");

      $('a[data-modal-id]').click(function(e) {
        e.preventDefault();
        $("body").append(appendthis);
        $(".modal-overlay").fadeTo(500, 0.7);
        //$(".js-modalbox").fadeIn(500);
        var modalBox = $(this).attr('data-modal-id');
        $('#'+modalBox).fadeIn($(this).data());
      });  
       $('button.clickme').click(function(e) {
            e.preventDefault();
            $("#popup").hide();
            $(".modal-overlay").hide();
        }); 
      
    $(".js-modal-close, .modal-overlay").click(function() {
      $(".modal-box, .modal-overlay").fadeOut(500, function() {
        $(".modal-overlay").remove();
      });
    });
     
    $(window).resize(function() {
      $(".modal-box").css({
        top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
        left: ($(window).width() - $(".modal-box").outerWidth()) / 2
      });
    });
     
    $(window).resize();
     
    });
});


// $(document).ready(function(){

//  $('a').click(function() {
//  $('html, body').animate( {
//      scrollTop: $( $.attr(this, 'href') ).offset().top
//  }, 1000);
//  return false;
//  });

// });

$(document).ready(function(){       
    var scroll_pos = 0;
    $(document).scroll(function() { 
        scroll_pos = $(this).scrollTop();
        if(scroll_pos > 65) {
            $(".contain-header").css('background-color', '#d32f2f');
        } else {
            $(".contain-header").css('background-color', 'rgba(38, 39, 43, 0.05)');
        }
    });
});

