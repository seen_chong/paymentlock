<?php get_header(); ?>

	<main role="main">

		<section id="slider">
			<div class="slider-container">

            <?php
	  			$args = array(
	    		'post_type' => 'homepage-banners'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>
				<div class="slider-visual" style="background-image:url(<?php the_field('banner'); ?>); background-size: cover; background-repeat: no-repeat;">
					<div class="slider-overlay">
						<h2><?php the_field('text'); ?></h2>
						<a class="js-open-modal" href="#" data-modal-id="popup"><button class="btn-getstarted-sm">Get Started > </button></a>
					</div>
				</div>

			<?php
				}
					}
				else {
				echo 'No Events Found';
				}
			?>

<!-- 				<div class="slider-visual" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/judge-jury.jpeg); background-size: cover; background-repeat: no-repeat;">

					<div class="slider-overlay">
						<h2>We support law firms and financial institutions.</h2>
						<a href=""><button class="btn-getstarted-sm">Get Started > </button></a>
					</div>
				</div>

				<div class="slider-visual" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/real-estate-firm.jpg); background-size: cover; background-repeat: no-repeat;">

					<div class="slider-overlay">
						<h2>We support Real Estate Firms & Commercial Businesses.</h2>
						<a href=""><button class="btn-getstarted-sm">Get Started > </button></a>
					</div>
				</div>
				<div class="slider-visual" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/gaming.jpg); background-size: cover; background-repeat: no-repeat;">

					<div class="slider-overlay">
						<h2>Gaming & Hospitality, Commercial Businesses, HealthCare and Dentistry, Sports Venues, Law Offices, Night Life & Bar/Restaurants – “paymentLOCK is right for any vertical"</h2>
						<a href=""><button class="btn-getstarted-sm">Get Started > </button></a>
					</div>
				</div> -->
			</div>
				<div class="slider-quote">
					<h3>paymentLOCK provides a layer of security within our payment architecture that has substantially reduced our corporate PCI scope over the past year and given our team the flexibility to receive payments in real-time</h3>
				</div>
			
		</section>

		<section class="spotlight" id="features">

			<div class="spotlight-solutions">
				<h4>Explore our card-not-present solutions</h4>
				<ul>

					<?php
					  $args = array(
					    'post_type' => 'features'
					    );
					  $products = new WP_Query( $args );
					  if( $products->have_posts() ) {
					    while( $products->have_posts() ) {
					      $products->the_post();
					?>
					
						<a class="link" href="#" data-rel="content<?php the_field('id'); ?>">
							<li>
							<img src="<?php echo get_template_directory_uri(); ?>/img/pos-icon.png">
							<h5><?php the_field('feature'); ?></h5>
							</li>
						</a>
					
					<?php
				    		}
				  		}
					  else {
					    echo 'No Features Found';
					  }
				  	?>
				</ul>
			</div>

				<div class="screen-box"> 
					<h3>Real-Time Data</h3>
					<img src="http://beta.paymentlock.com/wp-content/uploads/2016/09/RealTimeData.jpg">
					<p>Manage all of your transactions in real time through our customized cloud based dashboard. See approved authorizations, charge for services in real-time, store tokenized data for service charges later, setup recurring billing and much more.</p>
				</div>	

				<?php
				  $args = array(
				    'post_type' => 'features'
				    );
				  $products = new WP_Query( $args );
				  if( $products->have_posts() ) {
				    while( $products->have_posts() ) {
				      $products->the_post();
				?>

				<div class="screen-box" id="content<?php the_field('id'); ?>" style="display:none;"> 
					<h3><?php the_field('feature'); ?></h3>
					<img src="<?php the_field('image'); ?>">
					<p><?php the_field('content'); ?> </p>
				</div>	
				<?php
			    		}
			  		}
				  else {
				    echo 'No Features Found';
				  }
			  	?>



			<div class="spotlight-features">
			<?php
	  			$args = array(
	    		'post_type' => 'solutions',
	    		
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>
				<div class="featured-spot">
					<img src="<?php echo get_template_directory_uri(); ?>/img/dash-view.png">
					<h4><?php the_field('title'); ?></h4>
					<p><?php the_field('text'); ?></p>
					<a href=""><button class="btn-visit">Visit your dashboard</button></a>
				</div>
		<?php
			}
				}
			else {
			echo 'No Solutions Found';
			}
		?>


			</div>
		</section>

		<section id="hero">

			<div class="hero-visual">
				<img src="<?php echo get_template_directory_uri(); ?>/img/hero-img-1.jpg">
				<div class="hero-overlay">
					<h2>Know your Customers More...<br> So you can Transact More.</h2>
					<p>Tired of waiting for invoicing, scans, faxes and emails to capture non-secure cardholder data??</p>
					<p>Don’t expose your business, securely transact with you customers immediately. </p>
					<a class="js-open-modal" href="#" data-modal-id="popup"><button class="btn-getstarted-sm">Get Started > </button></a>				
				</div>
			</div>
		</section>

		<section id="pricing" id="pricing">
			<div class="pricing-header">
				<h2>Monthly Flat Rate Fee Pricing</h2>
				<p>No hidden monthly fees! Realize new revenue from new and existing customers immediately. </p>
<!-- 				<img src="<?php echo get_template_directory_uri(); ?>/img/price-border.png"> -->
			</div>

			<div class="pricing-plans">
				<div class="monthly-plan">
					<h3><span class="thin-font"></span></h3>
					<h4><span class="tiny-font"></span></h4>
					<p></p>
					<ul>
						<li><img src="<?php echo get_template_directory_uri(); ?>/img/cloud-icon.png"><b>Simple.</b> Easy setup and no hidden fees</li>
						<li><img src="<?php echo get_template_directory_uri(); ?>/img/setting-icon.png"><b>Flexible.</b> Pay only for what you use</li>
						<li><img src="<?php echo get_template_directory_uri(); ?>/img/lock-icon.png"><b>Secure.</b> Depend on a highly safe cloud-based solution</li>
					</ul>
					<a href="https://paymentlock.zendesk.com/hc/en-us" target="_blank"><button>Create Account ></button></a>
				</div>
				<div class="enterprise-plan">
					<h3>Large Enterprise</h3>
					<p>PaymentlOCK offers everything an Enterprise company would need to deliver a better and more secure customer experience all while making it easier to realize immediate funds. </p>
					<ul>
						<li><img src="<?php echo get_template_directory_uri(); ?>/img/circle-arrow.png"><br>Lower Rates</li>
						<li><img src="<?php echo get_template_directory_uri(); ?>/img/circle-phone.png"><br>24hr Support</li> 
					</ul>
					<a class="js-open-modal" href="#" data-modal-id="popup"><button>Contact Sales</button></a>
				</div>
			</div>
			
		</section>

		<section id="closer">
												<?php
			  			$args = array(
			    		'post_type' => 'testimonials',
			    		
			    		);
			  			$products = new WP_Query( $args );
			  				if( $products->have_posts() ) {
			    			while( $products->have_posts() ) {
			      		$products->the_post();
					?>
			<div class="test-slider">


				<div class="test-container">
					<div class="closer-title">
						<h1>Test-Drive our <strong>Industry Leading Solution</strong> for <span id="red">card-not-present</span> transactions.</h1>
					</div>



					<div class="closer-quote">
						<img src="<?php the_field('logo'); ?>">
						<div class="quote-bubble">
							<h6><?php the_field('quote'); ?></h6>
						</div>
					</div>

					<div class="closer-section">

						<div class="spotlight-square">
							<h4>Testimonial 1</h4>
							<p>“PaymentLOCK has substantially driven more revenue with our marketing and sales divisions by allowing our team to interact with our customers quickly and efficiently. PaymentLOCK continues to drive more revenue to our bottom line each month” – <strong>Hospitality Group</strong></p>
						</div>
						<div class="spotlight-square">
							<h4>Testimonial 2</h4>
							<p>“Security is paramount for our business and paymentLOCK provides critical security around all of our card-not-present transactions corporate wide. Removing this sensitive data from our corporate environment is a key part or our security posture” – <strong>Enterprise F&B</strong></p>
						</div>
						<div class="spotlight-square free-demo">
							<h4>Testimonial 3</h4>
							<p>“PaymentLOCK has been a game changer for our finance department. All online transactions have been secured and consolidated for reporting. We no longer worry about faxes, emails, or phone payments” – <strong>Real Estate Firm</strong></p>
						</div>



						<div class="spotlight-square">
							<h5>Every transaction that's not secured is a potential risk for your company.</h5>
							<a class="js-open-modal" href="#" data-modal-id="popup"><button class="btn-getstarted-med">Get Started with a free demo</button></a>
						</div>
					</div>
				</div>
											<?php
						}
							}
						else {
						echo 'No Solutions Found';
						}
					?>	
			</div>

		</section>

	</main>

	<div id="popup" class="modal-box">  
	<div class="popup-left">
		<img src="<?php echo get_template_directory_uri(); ?>/img/logo-paymentlock.png" alt="Logo" class="popup-logo">
		<h2>Try the #1 software for processing and securing card-not-present transactions.</h2>
		<p>Questions? Call us at <strong>1-800.576.3842</strong></p>
		<ul>
			<li>Receive payments smarter with paymentLOCK</li>
			<li>Save money with better PCI compliance.</li>
			<li>Authorize more efficiently with realtime transaction.</li>
		</ul>
	</div>
	<div class="popup-right">
		<div class="contact-form-header">
			<button class="btn-getstarted-sm clickme">Close</button></a>
			<h3>Schedule a call or demo</h3>
			<p>It's easy. Just fill in the fields below.</p>
		</div>
		<div class="contact-form">
			<?php echo do_shortcode('[contact-form-7 id="40" title="Schedule Demo"]'); ?>
		</div>
	</div>
</div>


<?php get_footer(); ?>

<script type="text/javascript">
$(document).ready(function(){
  $('.slider-container').slick({
    autoplay: true,
    autoplaySpeed: 5000,
      responsive: [
    {
      breakpoint: 1024,
      settings: {
        infinite: true,
      }
    }
    ]
  });
});

</script>

<script type="text/javascript">
	$(document).ready(function(){
  $('.test-slider').slick({
    autoplay: true,
    autoplaySpeed: 5000,
    adaptiveHeight: true
  });
});	
</script>

<script type="text/javascript">
jQuery(".link").click(function(e) {
    e.preventDefault();
    jQuery('.screen-box').hide();
    jQuery('#' + jQuery(this).data('rel')).slideDown();
});
</script>
