			<!-- footer -->
			<footer class="footer" role="contentinfo">
				<div class="footer-wrapper">
					<!-- copyright -->
					<img class="footer-logo" src="<?php echo get_template_directory_uri(); ?>/img/footer-logo.png">
					<p class="copyright">
						Copyright &copy;2016 paymentLock. All rights reserved.
					</p>	
					<a href="tel:1-877-464-3001"><button class="btn-contactus">Contact us</button></a>

					<!-- /copyright -->
				</div>

				<div class="footer-contact">
					<div class="contact-box">
						<div class="top-box">
							<h4>Get in Touch</h4>
							<p>ENTER YOUR EMAIL... we'll contact you in 1 hr or less</p>
							<input type="text" class="email-input"></input>
							<input type="submit" value="Go >" class="email-go"></input>
						</div>
						<div class="bottom-box">
							<a href="">Call Us - (877) 464-3001</a>
						</div>
					</div>
				</div>
			</footer>
			<!-- /footer -->

		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>

		<!-- analytics -->

	</body>
</html>
