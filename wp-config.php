<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'paymentlock_dev');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'kU :b!MU4iL@vs`H5k7V`d!Os2Z^TEz]SpR*?DddNY6cm);CP2RRvOqdv.EVMepH');
define('SECURE_AUTH_KEY',  'ljdPPgz(%i/68D5)UnV(+#(ic*d4-kv<Q}LvE5hYm[6{c*>9}:9)ZT|Q(uKKr~`8');
define('LOGGED_IN_KEY',    'xP(CN*]U7K.>[|WuNI)*+zTu! ^x5N$^>hX<T/:?Oi#k,YxesbLV`w`=#xq1$_7-');
define('NONCE_KEY',        '%ZeAQ)n8 d*.|Cte2eh8OW&*mM{dQy<ow;f&?WExSje==s](>zK(C@=ivEL~;y1s');
define('AUTH_SALT',        'i3?z[]opMl3trk#||+kex+M%VQ!Vm}5O)K%n&|jQsvf`&DMN6[?x%:CoWr+_~{rB');
define('SECURE_AUTH_SALT', ',(%{~P$O$tS#XR@2gUm*2Bp0i3U6W^~BeaDqySwx5kF5Y: ,Y{pxcYl5X5*Uq(fj');
define('LOGGED_IN_SALT',   '(.9.XCGvd l8HBSRG0P~,vbLUwG*fK}+R<vq@-BRY<44v{L8?1SJNr0CpWZlb3F8');
define('NONCE_SALT',       'oHG*w+_ q5/j(N0cC}&tXo|7%?@U{7+FN[W #$9E9F7a|M[a!K*)HrV~Tq227[>^');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
